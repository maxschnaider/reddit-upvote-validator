import firebase from '../services/firebase'
import store from '../store'

export default class User {
    constructor(data) {
        data = typeof data === "object" && data !== null ? data : {}
        Object.defineProperty(this, 'id', {
            value: data['.key'] !== undefined ? data['.key'] : data.id !== undefined ? data.id : data.username,
            writable: true
        })
        this._username = data.username
        Object.defineProperty(this, "username", {
            set: function(v) {
                this._username = v
                this.update('username', v)
            },
            get: function() {
                return this._username || ''
            }
        })
        this._created = data.created
        Object.defineProperty(this, "created", {
            set: function(v) {
                this._created = v
                this.update('created', v)
            },
            get: function() {
                return this._created || null
            }
        })
        this._karma = data.karma
        Object.defineProperty(this, "karma", {
            set: function(v) {
                this._karma = v
                this.update('karma', v)
            },
            get: function() {
                return this._karma || null
            }
        })
        this.days = this.username ? Math.ceil(Math.abs(Date.now() - this.created) / (1000 * 60 * 60 * 24)) : null
        this._jobs = data.jobs
        Object.defineProperty(this, "jobs", {
            set: function(v) {
                this._jobs = v
                this.update('jobs', v)
            },
            get: function() {
                return this._jobs || {}
            }
        })
        this.admin = data.admin
    }

    async fetch() {
        if (this.id !== undefined) {
            const res = await firebase.database().ref(`users/${this.id}`).once('value')
            const data = res.val()
            if (data) {
                this._username = data.username
                this._karma = data.karma
                this._created = new Date(Date.parse(data.created))
                this._jobs = data.jobs
                this.admin = data.admin
            }
        }
        return this
    }

    toJSON() {
        return {
            'username': this.username,
            'karma': this.karma,
            'created': this.created,
            'days': this.days,
            'jobs': this.jobs
        }
    }

    update(key, value) {
        if (this.id && value !== undefined)
            return firebase.database().ref(`users/${this.id}/${key}`).set(value)
    }

    save() {
        if (this.id)
            return firebase.database().ref(`users/${this.id}`).update(this.toJSON())
    }

    remove() {
        if (this.id)
            return firebase.database().ref(`users/${this.id}`).set(null)
    }
}
