import firebase from '../services/firebase'

export default class Task {
    constructor(data) {
        data = typeof data === "object" && data !== null ? data : {}
        Object.defineProperty(this, 'id', {
            value: data['.key'] !== undefined ? data['.key'] : data.id !== undefined ? data.id : 0,
            writable: true
        })
        this._url = data.url
        Object.defineProperty(this, "url", {
            set: function(v) {
                this._url = v
                this.update('url', v)
            },
            get: function() {
                return this._url ? this._url.includes('https://') ? this._url : 'https://'+this._url : ''
            }
        })
        this._text = data.text
        Object.defineProperty(this, "text", {
            set: function(v) {
                this._text = v
                this.update('text', v)
            },
            get: function() {
                return this._text || ''
            }
        })
        Object.defineProperty(this, 'textHtml', {
            value: this.text.replace('URL', `<a href=${this.url} target="_blank">${this.url.replace('https://', '')}</a>`),
            writable: false
        })
        this._users = data.users
        Object.defineProperty(this, "users", {
            set: function(v) {
                this._users = v
                this.update('users', v)
            },
            get: function() {
                return this._users || []
            }
        })
    }

    toJSON() {
        return {
            'url': this.url,
            'text': this.text,
            'users': this.users
        }
    }

    getLink() {
        return window.location.origin+'?task='+this.id
    }

    async fetch() {
        if (this.id !== undefined) {
            const res = await firebase.database().ref(`tasks/${this.id}`).once('value')
            const data = res.val()
            if (data) {
                this._text = data.text
                this._url = data.url
                this._users = data.users
            }
        }
        return this
    }

    update(key, value) {
        if (this.id !== undefined && value !== undefined)
            return firebase.database().ref(`tasks/${this.id}/${key}`).set(value)
    }

    save() {
        if (this.id !== undefined)
            return firebase.database().ref(`tasks/${this.id}`).update(this.toJSON())
    }

    remove() {
        if (this.id !== undefined)
            return firebase.database().ref(`tasks/${this.id}`).set(null)
    }
}
