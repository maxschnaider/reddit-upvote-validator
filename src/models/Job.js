import firebase from '../services/firebase'

export default class Job {
    constructor(data) {
        data = typeof data === "object" && data !== null ? data : {}
        Object.defineProperty(this, 'id', {
            value: data['.key'] !== undefined ? data['.key'] : data.id !== undefined ? data.id : 0,
            writable: true
        })
        this.taskId = data.taskId
        this.username = data.username
        this.datetime = data.datetime
    }

    getLink() {
        return window.location.origin+'?job='+this.id
    }

    toJSON() {
        return {
            'taskId': this.taskId,
            'username': this.username,
            'datetime': this.datetime
        }
    }

    async fetch() {
        if (this.id !== undefined) {
            const res = await firebase.database().ref(`jobs/${this.id}`).once('value')
            const data = res.val()
            if (data) {
                this.taskId = data.taskId
                this.username = data.username
                this.datetime = data.datetime
            }
        }
        return this
    }

    update(key, value) {
        if (this.id !== undefined && value !== undefined)
            return firebase.database().ref(`jobs/${this.id}/${key}`).set(value)
    }

    save() {
        if (this.id !== undefined)
            return firebase.database().ref(`jobs/${this.id}`).update(this.toJSON())
    }

    remove() {
        if (this.id !== undefined)
            return firebase.database().ref(`jobs/${this.id}`).set(null)
    }
}
