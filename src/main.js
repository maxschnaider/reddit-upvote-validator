import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import VueCookies from 'vue-cookies'

Vue.config.productionTip = false
Vue.use(VueCookies)
Vue.$cookies.config('30d')

let app = new Vue({
    router,
    store,
    render: h => h(App)
})
app.$mount('#app')
