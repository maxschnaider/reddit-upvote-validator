import Vue from 'vue'
import Vuex from 'vuex'
import { vuexfireMutations, firebaseAction } from 'vuexfire'
import firebase from '../services/firebase'

import User from '../models/User'
import Task from '../models/Task'
import Job from '../models/Job'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        user: {},
        task: {},
        job: {},
        tasks: []
    },
    getters: {
        user(state) {
            return new User(state.user)
        },
        task(state) {
            return new Task(state.task)
        },
        job(state) {
            return new Job(state.job)
        },
        tasks(state) {
            return state.tasks.map(taskData => { return new Task(taskData) })
        }
    },
    mutations: {
        ...vuexfireMutations,
        setUser(state, data) {
            state.user = data
        },
        setTask(state, data) {
            state.task = data
        },
        setJob(state, data) {
            state.job = data
        }
    },
    actions: {
        bindFirebaseCollections: firebaseAction(({ state, bindFirebaseRef }, collections) => {
            for (const collection of collections) {
                bindFirebaseRef(collection, firebase.database().ref(collection))
            }
        }),
        unbindFirebaseCollections: firebaseAction(({ state, unbindFirebaseRef }, collections) => {
            for (const collection of collections) {
                unbindFirebaseRef(collection, firebase.database().ref(collection))
            }
        }),
    }
})
