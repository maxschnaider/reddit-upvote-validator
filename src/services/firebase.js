import * as firebase from "firebase"

const firebaseConfig = {
    apiKey: "AIzaSyDkIDL47WqTpEjO0UAZn47u6wA7BjIQyGw",
    authDomain: "reddit-booster.firebaseapp.com",
    databaseURL: "https://reddit-booster-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "reddit-booster",
    storageBucket: "reddit-booster.appspot.com",
    messagingSenderId: "750151288094",
    appId: "1:750151288094:web:5a1f8f7b67c73e3f89350f",
    measurementId: "G-LBMBFHJ4NK"
};

const app = firebase.initializeApp(firebaseConfig)
export default app
