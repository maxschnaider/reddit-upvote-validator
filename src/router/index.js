import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import firebase from "../services/firebase"

import Main from '../pages/Main.vue'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '*',
            name: 'Main',
            component: Main,
            meta: {
                requiresAuth: false
            }
        }
    ]
})

router.beforeEach(async (to, from, next) => {
    let currentUser
    // if (!store.state.user.loggedIn) {
    //     currentUser = await firebase.auth().currentUser
    // } else {
    //     currentUser = true
    // }
    next()
})

export default router
